#include "day-of-year.hpp"

int dayOfYear(int month, int dayOfMonth, int year) {
    if(month > 1) dayOfMonth += 31;
    if(month > 2)
    {
        if(year % 4 == 0 and (year % 100 != 0 or year % 400 == 0))
        {
            dayOfMonth += 29;
        }
        else dayOfMonth += 28;
    }
    if(month > 3) dayOfMonth += 31;
    if(month > 4) dayOfMonth += 30;
    if(month > 5) dayOfMonth += 31;
    if(month > 6) dayOfMonth += 30;
    if(month > 7) dayOfMonth += 31;
    if(month > 8) dayOfMonth += 31;
    if(month > 9) dayOfMonth += 30;
    if(month > 10) dayOfMonth += 31;
    if(month > 11) dayOfMonth += 30;

    return dayOfMonth;
}

