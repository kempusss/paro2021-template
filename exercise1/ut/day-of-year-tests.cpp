#include "gtest/gtest.h"
#include "day-of-year.hpp"

struct DayOfYearTestSuite {};

TEST(DayOfYearTestSuite, dummyTest)
{
  ASSERT_TRUE(true);
}

TEST(DayOfYearTestSuite, January1stIsFitstDayOfYear)
{
  ASSERT_EQ(dayOfYear(1, 1, 2020), 1);
}

TEST(DayOfYearTestSuite, lastDayofLeapYear)
{
  ASSERT_EQ(dayOfYear(12, 31, 2020), 366);
}

TEST(DayOfYearTestSuite, lastDayofYear)
{
  ASSERT_EQ(dayOfYear(12, 31, 2021), 365);
}

TEST(DayOfYearTestSuite, randomTest1)
{
  ASSERT_EQ(dayOfYear(6, 16, 2021), 167);
}

TEST(DayOfYearTestSuite, randomTest2)
{
  ASSERT_EQ(dayOfYear(7, 15, 1410), 196);
}

TEST(DayOfYearTestSuite, notRealyLeapYear)
{
  ASSERT_EQ(dayOfYear(12, 31, 1900), 365);
}

TEST(DayOfYearTestSuite, realyLeapYear)
{
  ASSERT_EQ(dayOfYear(12, 31, 2000), 366);
}
