#include "gtest/gtest.h"
#include "scrabble-score.hpp"

struct ScrabbleScoreTestSuite {};

TEST(ScrabbleScoreTestSuite, dummyTest)
{
  ASSERT_TRUE(true);
}

TEST(ScrabbleScoreTestSuite, singleLetterTest)
{
  ASSERT_EQ(scrabbleScore("A"), 1);
  ASSERT_EQ(scrabbleScore("D"), 2);
  ASSERT_EQ(scrabbleScore("B"), 3);
  ASSERT_EQ(scrabbleScore("F"), 4);
  ASSERT_EQ(scrabbleScore("K"), 5);
  ASSERT_EQ(scrabbleScore("J"), 8);
  ASSERT_EQ(scrabbleScore("Q"), 10);
}

TEST(ScrabbleScoreTestSuite, wordTest)
{
  ASSERT_EQ(scrabbleScore("CABBAGE"), 14);
}

TEST(ScrabbleScoreTestSuite, wordTestRandomCase)
{
  ASSERT_EQ(scrabbleScore("CaBBagE"), 14);
}

TEST(ScrabbleScoreTestSuite, wordEmpty)
{
  ASSERT_EQ(scrabbleScore(""), 0);
}
